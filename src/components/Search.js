import React, { useContext, useState } from 'react'
import { collection, doc, getDoc, getDocs, query, serverTimestamp, setDoc, updateDoc, where } from "firebase/firestore";
import "../style.scss";
import { db } from '../firebase';
import { AuthContext } from '../context/AuthContext';
const Search = () => {
  const [username, setUsername ] = useState("");
  const [user, setUser] = useState(null);
  const [err, setErr] = useState(false);
  const {currentUser} = useContext(AuthContext);

  const handleSearch = async () => {
    try{
      const q = query(collection(db, "user"), where("displayName", "==", username));

      const querySnapshot = await getDocs(q);
      if(querySnapshot._snapshot.docChanges.length === 0) {
        setUser(null)
        setErr(true);
        return;
      }
      querySnapshot.forEach((doc) => {
        // doc.data() is never undefined for query doc snapshots
        // console.log(doc.id, " => ", doc.data());
        setUser(doc.data());
      });
      setErr(false)
    }catch(error) {
      console.log(error);
      setErr(true);
    }
  }

  const handleKey = (e) => {
    e.code === "Enter" && handleSearch();
  }

  const handleSelect = async () => {
    // Check wheter the group (chat in firestore) exists. if not create new one.
    const combinedId = currentUser.uid > user.uid ? currentUser.uid + user.uid : user.uid + currentUser.uid;
    try {
      const res = await getDoc(doc(db, "chats", combinedId));

      if(!res.exists()) {
        await setDoc(doc(db, "chats", combinedId), {messages: []});
        await updateDoc(doc(db, "userChat", currentUser.uid), {
          [combinedId + ".userInfo"]: {
            uid: user.uid,
            displayName: user.displayName,
            photoURL: user.photoURL
          },
          [combinedId+".date"]: serverTimestamp()
        });
        await updateDoc(doc(db, "userChat", user.uid), {
          [combinedId + ".userInfo"]: {
            uid: currentUser.uid,
            displayName: currentUser.displayName,
            photoURL: currentUser.photoURL
          },
          [combinedId+".date"]: serverTimestamp()
        });
      }
      setUser(null);
      setUsername("");
    }catch(error) {
      console.log(error);
      setUser(null);
      setUsername("");
    }
  }

  return (
    <div className='search'>
      <div className='searchForm'>
        <input
          type="text"
          placeholder='Find a user'
          onKeyDown={handleKey}
          onChange={e => setUsername(e.target.value)}
          value={username}
        />
      </div>
      {err && <span className='errMsg'>User not found</span>}
      {
        user && 
        (
          <div 
            className='userChat'
            onClick={handleSelect}
          >
            <img 
              src={user.photoURL}
              alt="seach_image"
            />
            <div className='userChatInfo'>
              <span>{user.displayName}</span>
            </div>
          </div>
        )
      }
    </div>
  )
}

export default Search