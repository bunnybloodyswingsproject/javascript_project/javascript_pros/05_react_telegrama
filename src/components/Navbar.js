import { signOut } from 'firebase/auth';
import React, { useContext } from 'react'
import { AuthContext } from '../context/AuthContext';
import { auth } from '../firebase';
import "../style.scss";

const Navbar = () => {
  const { currentUser } = useContext(AuthContext);

  return (
    <div className='navbar'>
      <div className='user'>
        <div className='user_profie'>
          <img
            src={currentUser.photoURL}
            alt="chat_logo"
            />
        </div>
        <button onClick={() => signOut(auth)}>
          <i className="fa-solid fa-door-open"></i>
        </button>
      </div>
    </div>
  )
}

export default Navbar