// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";
import { getFirestore } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDcjgjCmYIsxqgdzcdV4PjxNbkRDuo9cvM",
  authDomain: "chat-assestment.firebaseapp.com",
  projectId: "chat-assestment",
  storageBucket: "chat-assestment.appspot.com",
  messagingSenderId: "478825230394",
  appId: "1:478825230394:web:f5e833261355d1058d4503",
  measurementId: "G-DFC7EEP02Q"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const storage = getStorage();
export const db = getFirestore();