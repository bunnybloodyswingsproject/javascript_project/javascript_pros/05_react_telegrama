import React, { useContext } from 'react'
import "../style.scss";
import Messages from './Messages';
import Input from "./Input";
import { ChatContext } from '../context/ChatContext';

const Chat = () => {
  const { data } = useContext(ChatContext)
  console.log(data);
  return (
    <div className='chat'>
      {
        data.chatId !== "null" ?
        (
          <>
            <div className='chatInfo'>
              <span>{data.user?.displayName}</span>
              <div className='chatIcons'>
                <i className="fa-solid fa-video"></i>
                <i className="fa-solid fa-user-plus"></i>
                <i className="fa-solid fa-ellipsis"></i>
              </div>
            </div>
            <Messages />
            <Input />
          </>
        )
        :
        (
          <div className='tech_stacks'>
            <div className='stack_header'>
              <h1>Chat Stacks</h1>
              <div className='sticks'></div>
            </div>
            <div className='stack_contents'>
              <div className='tect_cards'>
                <div className='stack_image'>
                  <img
                    src="https://firebasestorage.googleapis.com/v0/b/chat-assestment.appspot.com/o/5847f40ecef1014c0b5e488a.png?alt=media&token=75420e84-9e80-40fa-9cc8-38a512e2bbc7"
                    alt="stack_image"
                  />
                </div>
                <div className='stack_description'>
                  <p>Google Firebase</p>
                  <small>Backend</small>
                  <div className='progress_container'>
                    <div className='progress_bar'></div>
                  </div>
                  <p className='progress_percentage'>50%</p>
                </div>
              </div>
              <div className='tect_cards'>
                <div className='stack_image'>
                  <img
                    src="https://firebasestorage.googleapis.com/v0/b/chat-assestment.appspot.com/o/reactjs-removebg-preview.png?alt=media&token=c35c9a58-5e49-41b8-9f7f-52e911ca4ebc"
                    alt="stack_image"
                  />
                </div>
                <div className='stack_description'>
                  <p>ReactJS Framework</p>
                  <small>Frontend Framework</small>
                  <div className='progress_container'>
                    <div className='progress_bar' id="reactjs_progress"></div>
                  </div>
                  <p className='progress_percentage'>30%</p>
                </div>
              </div>
              <div className='tect_cards'>
                <div className='stack_image'>
                  <img
                    src="https://firebasestorage.googleapis.com/v0/b/chat-assestment.appspot.com/o/sass-removebg-preview.png?alt=media&token=c41cb89a-5a49-4ecb-b300-2c89b722651f"
                    alt="stack_image"
                  />
                </div>
                <div className='stack_description'>
                  <p>Sass</p>
                  <small>CSS Framework</small>
                  <div className='progress_container'>
                    <div className='progress_bar' id="sass_progress"></div>
                  </div>
                  <p className='progress_percentage'>20%</p>
                </div>
              </div>
            </div>
          </div>
        )
      }
    </div>
  )
}

export default Chat