import { 
  getStorage, 
  ref, 
  uploadBytesResumable, 
  getDownloadURL 
} from "firebase/storage";
import React, { useState } from 'react'
import "../style.scss";
import Add from "../img/addAvatar.png"
import { createUserWithEmailAndPassword, updateProfile } from "firebase/auth";
import { auth, db } from '../firebase';
import { storage } from "../firebase";
import { doc, setDoc } from "firebase/firestore";
import { Link, useNavigate } from "react-router-dom";

const Register = () => {
  const [err, setErr] = useState(false);
  const navigate = useNavigate();
  
  const handleSumbit = async (e) => {
    e.preventDefault();
    const displayName = e.target[0].value;
    const email = e.target[1].value;
    const password = e.target[2].value;
    const file = e.target[3].files[0];
    try {
      const res = await createUserWithEmailAndPassword(auth, email, password);
      const storageRef = ref(storage, displayName);

      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on('state_changed', 
        (snapshot) => {

        }, 
        (error) => {
          // Handle unsuccessful uploads
          setErr(true);
        }, 
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
            // console.log('File available at', downloadURL);
            await updateProfile(res.user, {
              displayName,
              photoURL: downloadURL
            })
            await setDoc(doc(db, "user", res.user.uid), {
              uid: res.user.uid,
              displayName,
              email,
              photoURL: downloadURL
            });

            await setDoc(doc(db, "userChat", res.user.uid), {})
            navigate("/");
          });
        }
      );

    }catch(error) {
      setErr(prev => !prev);
    }
  }

  return (
    <div className='formContainer'>
      <div className='formWrapper'>
        <span className='logo'>Telegrama</span>
        <span className='title'>Register</span>
        <form onSubmit={handleSumbit}>
          <input
            type="text"
            placeholder='display name'
          />
          <input
            type="email"
            placeholder='email'
          />
          <input
            type="password"
            placeholder='password'
          />
          <input
            type="file"
            placeholder='file'
            id="file"
            style={{
              display: "none"
            }}
          />
          <label htmlFor='file'>
            <img 
              src={Add}
              alt="addAavatar"
            />
            <span> Add an avatar</span>
          </label>
          <button>Sign Up</button>
          { err && <span>Something went wrong</span> }
        </form>
        <p>You do have an account? <Link to="/login">Login</Link></p>
      </div>
    </div>
  )
}

export default Register