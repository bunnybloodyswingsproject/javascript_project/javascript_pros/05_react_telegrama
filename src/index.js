import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { AuthProvider } from './context/AuthContext';
import { ChatProvider } from './context/ChatContext';
import { Partytown } from "@builder.io/partytown/react";
import PartytownIntegration from './hooks/PartytownIntegration';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <>
  <Partytown debug={process.env.REACT_APP_NODE_ENV === "development"} forward={["dataLayer.push"]} />
  <PartytownIntegration>
    <AuthProvider>
      <ChatProvider>
        <React.StrictMode>
          <App />
        </React.StrictMode>
      </ChatProvider>
    </AuthProvider>
  </PartytownIntegration>
  </>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
