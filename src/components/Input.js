import React, { useContext, useState } from 'react'
import { 
  ref, 
  uploadBytesResumable, 
  getDownloadURL 
} from "firebase/storage";
import { storage } from '../firebase';
import "../style.scss"
import { AuthContext } from '../context/AuthContext'
import { ChatContext } from '../context/ChatContext'
import { arrayUnion, doc, serverTimestamp, Timestamp, updateDoc } from 'firebase/firestore'
import { db } from '../firebase'
import { v4 as uuid } from 'uuid'
const Input = () => {
  const [ text, setText ] = useState("");
  const [image, setImage] = useState(null)
  const { currentUser } = useContext(AuthContext);
  const { data } = useContext(ChatContext);

  
  const handleSend = async (e) => {
    if(image) {
      const storageRef = ref(storage, uuid());
      const uploadTask = uploadBytesResumable(storageRef, image);

      uploadTask.on('state_changed', 
      (snapshot) => {

      }, 
      (error) => {
        // Handle unsuccessful uploads
        // setErr(true);
      }, 
      () => {
        // Handle successful uploads on complete
        // For instance, get the download URL: https://firebasestorage.googleapis.com/...
        getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
          // console.log('File available at', downloadURL);
          await updateDoc(doc(db, "chats", data.chatId), {
            messages: arrayUnion({
              id: uuid(),
              text,
              senderId: currentUser.uid,
              date: Timestamp.now(),
              img: downloadURL
            })
          })

        });
      }
    );

    }else{
      await updateDoc(doc(db, "chats", data.chatId), {
        messages: arrayUnion({
          id: uuid(),
          text,
          senderId: currentUser.uid,
          date: Timestamp.now()
        })
      })
    }

    await updateDoc(doc(db, "userChat", currentUser.uid), {
      [data.chatId + ".lastMessage"]: {
        text
      },
      [data.chatId + "data"]: serverTimestamp()
    });

    await updateDoc(doc(db, "userChat", data.user.uid), {
      [data.chatId + ".lastMessage"]: {
        text
      },
      [data.chatId + "data"]: serverTimestamp()
    });

    setText("");
    setImage(null)
  }

  return (
    <div className='input'>
      <input
        type="text"
        placeholder='Type something...'
        onChange={(e) => setText(e.target.value)}
        value={text}
      />
      <div className='send'>
        <i className="fa-solid fa-paperclip"></i>
        <input 
          type="file"
          style={{
            display: "none"
          }}
          id="file"
          onChange={(e) => setImage(e.target.files[0])}
        />
        <label htmlFor='file'>
          <i className="fa-regular fa-image"></i>
        </label>
        <button onClick={handleSend}><i className="fa-brands fa-telegram"></i></button>
      </div>
    </div>
  )
}

export default Input