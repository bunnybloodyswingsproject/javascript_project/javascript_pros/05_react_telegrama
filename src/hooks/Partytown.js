import { useEffect } from 'react'
import { partytownSnippet } from "@builder.io/partytown/integration";

const PartytownScript = () => {
  useEffect(() => {
    // if(!process.env.REACT_APP_NODE_ENV || process.env.REACT_APP_NODE_ENV === "development") {
    //     console.log("[DEV]: stubbing partytown integration script");
    //     return false;
    // }

    if(document) {
        const snippetText = partytownSnippet();
        const el = document.createElement("script");
        // el.type = "text/partytown";
        el.innerText = snippetText;
        // document.body.appendChild(el);
        // document.getElementsByTagName("head")[0].appendChild(el);
        document.body.appendChild(el);
        window.dispatchEvent(new CustomEvent("ptupdate"));
    }
  }, [])
}

export default PartytownScript