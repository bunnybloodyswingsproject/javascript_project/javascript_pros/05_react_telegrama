import React from 'react'
import PartytownScript from './Partytown'

const PartytownIntegration = ({children}) => {
  PartytownScript();
  return <>{children}</>
}

export default PartytownIntegration