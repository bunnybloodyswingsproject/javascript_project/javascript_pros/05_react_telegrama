import React, { useState } from 'react'
import { Link, useNavigate } from "react-router-dom";
import { auth } from '../firebase';
import { signInWithEmailAndPassword } from 'firebase/auth';
import "../style.scss";
const Login = () => {
  const [err, setErr] = useState(false);
  const [errMsg, setErrMsg] = useState("");
  const navigate = useNavigate();
  
  const handleSumbit = async (e) => {
    e.preventDefault();
    const email = e.target[0].value;
    const password = e.target[1].value;
    try {
      const response = await signInWithEmailAndPassword(auth, email, password);
      console.log(response);
      if(!response?.user) {
        setErr(prev => !prev);
        return 
      }
      setErrMsg("");
      setErr(false);
      navigate("/");
    } catch (err) {
      console.log(err.message);
      if(err.message === "Firebase: Error (auth/user-not-found).") {
        setErrMsg("Authentication Error: Invalid email or password.");
      }
      setErr(prev => !prev);
    }
  }

  return (
    <div className='formContainer'>
      <div className='formWrapper'>
        <span className='logo'>Telegrama</span>
        <span className='title'>Login</span>
        <form onSubmit={handleSumbit}>
          <input
            type="email"
            placeholder='email'
          />
          <input
            type="password"
            placeholder='password'
          />
          <button>Sign In</button>
          { err && <span className='errMsg'>{errMsg}</span> }
        </form>
        <p>You do'nt have an account? <Link to="/register">Register</Link></p>
      </div>
    </div>
  )
}

export default Login